﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Problema1
{
    public partial class HallFame : Form
    {
        Datos datos;
        List<int> puntuaciones;
        List<string> nombres;
        List<string> avatares;
        List<Label> lNombres;
        List<PictureBox> lAvatares;
        List<Label> lPuntuaciones;

        /// <summary>
        /// Constructor base
        /// </summary>
        public HallFame()
        {
            InitializeComponent();
            datos = Program.datos;
            puntuaciones = new List<int>(10);
            nombres = new List<string>(10);
            avatares = new List<string>(10);
            LeerDatos();
            lNombres = new List<Label>(10);
            lAvatares = new List<PictureBox>(10);
            lPuntuaciones = new List<Label>(10);
            LlenarListas();
            MostrarDatos();
        }

        /// <summary>
        /// Obtiene los datos y los separa en arrays
        /// </summary>
        private void LeerDatos()
        {
            List<Jugador> listaPuntos = datos.Puntuaciones;
            for (int i=0;i<10;i++)
            {
                nombres.Add(listaPuntos[i].nombre);
                puntuaciones.Add(listaPuntos[i].puntuacion);
                avatares.Add(listaPuntos[i].avatar);
            }
        }

        /// <summary>
        /// Agrega los controles del formulario a arrays, buscandolos mediante su propiedad Name
        /// </summary>
        private void LlenarListas()
        {
            for(int i=0;i<10;i++)
            {
                string labelNombre = string.Concat("labelN", i + 1);
                string labelPuntuacion = string.Concat("labelP", i + 1);
                string pictureAvatar = string.Concat("imgAvatar", + i + 1);
                PictureBox avatarControl = (PictureBox)Controls.Find(pictureAvatar, true).FirstOrDefault();
                Label nombreControl = (Label)Controls.Find(labelNombre, true).FirstOrDefault();
                Label puntuacionControl = (Label)Controls.Find(labelPuntuacion, true).FirstOrDefault();
                lNombres.Add(nombreControl);
                lPuntuaciones.Add(puntuacionControl);
                lAvatares.Add(avatarControl);
            }
        }

        /// <summary>
        /// Actualiza los controles del formulario con los datos obtenidos
        /// </summary>
        private void MostrarDatos()
        {
            for (int i=0;i<10;i++)
            {
                lNombres[i].Text = nombres[i];
                lPuntuaciones[i].Text = puntuaciones[i].ToString();
                lAvatares[i].Image = (Image)Properties.Resources.ResourceManager.GetObject(avatares[i]);
            }
        }
    }
}
