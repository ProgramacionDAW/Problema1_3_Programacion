﻿namespace Problema1
{
    class CeldaAcorazado:Celda
    {
        #region Constructor
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public CeldaAcorazado()
        {

        }

        /// <summary>
        /// Constructor usado para generar una celda de tipo acorazado
        /// </summary>
        /// <param name="fila"></param>
        /// <param name="columna"></param>
        /// <param name="id">Agrupa los barcos por ID</param>
        public CeldaAcorazado(int fila, int columna,int id) : base(fila, columna, id)
        {

        }
        #endregion

        #region Metodos
        /// <summary>
        /// Devuelve el estado de la celda
        /// </summary>
        /// <param name="formulario">Formulario base</param>
        /// <param name="jugador">Indica si es el jugador 1 o 2</param>
        /// <returns></returns>
        public override string Mostrar(Form1 formulario, int jugador)
        {
            Tablero tablero;
            if (jugador==1)
            {
                tablero = Program.juego.TableroJuego;
            }
            else
            {
                tablero = Program.juego2.TableroJuego;
            }
            string tipo = GetType().Name;
            if (tablero.BuscarCeldaID(this))
            {
                estado = Estados.Tocado;
            }
            else
            {
                estado = Estados.Hundido;
                formulario.ActualizarCantidadBarcos(tipo);
            }
            return Estado;
        }
        #endregion
    }
}
