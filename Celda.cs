﻿namespace Problema1
{
    abstract class Celda
    {
        #region Atributos
        protected enum Estados
        {
            Tocado,
            Hundido,
            Agua,
        }
        protected Estados estado;
        protected int fila;
        protected int columna;
        bool activo;
        protected int id;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor base
        /// </summary>
        public Celda()
        {
            activo = true;
        }

        /// <summary>
        /// Constructor usado para las celdas de tipo Agua y Submarino
        /// </summary>
        /// <param name="fila"></param>
        /// <param name="columna"></param>
        public Celda(int fila, int columna)
        {
            this.fila = fila;
            this.columna = columna;
            activo = true;
        }

        /// <summary>
        /// Constructor usado para el resto de celdas
        /// </summary>
        /// <param name="fila"></param>
        /// <param name="columna"></param>
        /// <param name="id"></param>
        public Celda(int fila, int columna, int id)
        {
            this.fila = fila;
            this.columna = columna;
            this.id = id;
            activo = true;
        }
        #endregion

        #region Metodos
        public abstract string Mostrar(Form1 formulario, int jugador);
        #endregion

        #region Propiedades
        /// <summary>
        /// Devuelve la fila
        /// </summary>
        public int Fila
        {
            get
            {
                return fila;
            }
        }

        /// <summary>
        /// Devuelve la columna
        /// </summary>
        public int Columna
        {
            get
            {
                return columna;
            }
        }

        /// <summary>
        /// Devuelve el ID
        /// </summary>
        public int ID
        {
            get
            {
                return id;
            }
        }

        /// <summary>
        /// Devuelve si la celda está activa
        /// </summary>
        public bool Activo
        {
            get
            {
                return activo;
            }
            set
            {
                activo = value;
            }
        }

        /// <summary>
        /// Devuelve el estado
        /// </summary>
        public string Estado
        {
            get
            {
                return estado.ToString();
            }
        }
        #endregion
    }
}
