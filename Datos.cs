﻿using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace Problema1
{
    class Datos
    {
        #region Atributos
        FileStream flujo;
        StreamWriter escritor;
        StreamReader lector;
        List<Jugador> datos;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor base
        /// </summary>
        public Datos()
        {
            try
            {
                flujo = new FileStream(@"FAME.hall", FileMode.Open, FileAccess.ReadWrite);
            }
            catch (FileNotFoundException) //Si no existe el archivo...
            {
                NuevoArchivo();
                flujo = new FileStream(@"FAME.hall", FileMode.Open, FileAccess.ReadWrite);
            }
            datos = new List<Jugador>(11); //ponemos 11 como máximo ya que vamos a agregar un jugador de más
            escritor = new StreamWriter(flujo);
            lector = new StreamReader(flujo);
            LeerDatos();
        }
        #endregion

        #region Metodos
        /// <summary>
        /// Genera un nuevo archivo en caso de que no exista
        /// </summary>
        private void NuevoArchivo()
        {
            FileStream archivo = new FileStream(@"FAME.hall", FileMode.CreateNew, FileAccess.ReadWrite);
            archivo.Close();
        }

        /// <summary>
        /// Lee y guarda los datos en un array
        /// </summary>
        private void LeerDatos()
        {
            for(int i=0;i<10;i++)
            {
                string datosLinea = lector.ReadLine();
                if (datosLinea==null || datosLinea=="")
                {
                    datosLinea = "NULL-000-interrogante";
                }
                string[] datosValores = datosLinea.Split('-');
                Jugador jugador;
                jugador.nombre = datosValores[0];
                jugador.puntuacion = int.Parse(datosValores[1]);
                jugador.avatar = datosValores[2];
                datos.Add(jugador);
            }
            escritor.Close();
            lector.Close();
            flujo.Close();
        }

        /// <summary>
        /// Agrega un registro al array y lo ordena
        /// </summary>
        /// <param name="nombre"></param>
        /// <param name="avatar"></param>
        /// <param name="puntuacion"></param>
        public void AgregarRegistro(string nombre, string avatar, int puntuacion)
        {
            Jugador jugador;
            jugador.nombre = nombre;
            jugador.puntuacion = puntuacion;
            jugador.avatar = avatar;
            datos.Add(jugador);
            datos = datos.OrderByDescending(x => x.puntuacion).ToList(); //Reordenamos el array por puntuación
        }

        /// <summary>
        /// Escribe los datos del array en el archivo
        /// </summary>
        public void EscribirDatos()
        {
            FileStream archivoFinal = new FileStream(@"FAME.hall", FileMode.Create, FileAccess.ReadWrite);
            archivoFinal.Seek(0, SeekOrigin.Begin);
            StreamWriter escritor = new StreamWriter(archivoFinal);
            for (int i=0;i<10;i++)
            {
                string linea = string.Concat(datos[i].nombre, "-", datos[i].puntuacion, "-", datos[i].avatar);
                if (i==9)
                {
                    escritor.Write(linea); //En la ultima linea hacemos un Write para que no escriba una linea vacia al final
                }
                else
                {
                    escritor.WriteLine(linea);
                }
            }
            escritor.Close();
            archivoFinal.Close();
        }
        #endregion

        #region Propiedades
        /// <summary>
        /// Devuelve la puntuación más baja
        /// </summary>
        public int PuntuacionMasBaja
        {
            get
            {
                return datos[9].puntuacion; //Puntuación del jugador 10
            }
        }

        /// <summary>
        /// Devuelve la lista de puntuaciones
        /// </summary>
        public List<Jugador> Puntuaciones
        {
            get
            {
                return datos;
            }
        }
        #endregion
    }
}
