﻿namespace Problema1
{
    partial class Registro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.contenedorAvatar = new System.Windows.Forms.FlowLayoutPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.PictSelec = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lPuntuacion = new System.Windows.Forms.Label();
            this.picLetra1 = new System.Windows.Forms.PictureBox();
            this.picLetra2 = new System.Windows.Forms.PictureBox();
            this.picLetra3 = new System.Windows.Forms.PictureBox();
            this.aumenta1 = new System.Windows.Forms.Panel();
            this.aumenta2 = new System.Windows.Forms.Panel();
            this.aumenta3 = new System.Windows.Forms.Panel();
            this.baja1 = new System.Windows.Forms.Panel();
            this.baja2 = new System.Windows.Forms.Panel();
            this.baja3 = new System.Windows.Forms.Panel();
            this.botonGuardar = new System.Windows.Forms.Button();
            this.contenedorAvatar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictSelec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLetra1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLetra2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLetra3)).BeginInit();
            this.SuspendLayout();
            // 
            // contenedorAvatar
            // 
            this.contenedorAvatar.AutoScroll = true;
            this.contenedorAvatar.BackColor = System.Drawing.Color.Transparent;
            this.contenedorAvatar.Controls.Add(this.pictureBox1);
            this.contenedorAvatar.Controls.Add(this.pictureBox2);
            this.contenedorAvatar.Controls.Add(this.pictureBox3);
            this.contenedorAvatar.Controls.Add(this.pictureBox4);
            this.contenedorAvatar.Controls.Add(this.pictureBox5);
            this.contenedorAvatar.Controls.Add(this.pictureBox6);
            this.contenedorAvatar.Controls.Add(this.pictureBox7);
            this.contenedorAvatar.Controls.Add(this.pictureBox8);
            this.contenedorAvatar.Controls.Add(this.pictureBox9);
            this.contenedorAvatar.Controls.Add(this.pictureBox10);
            this.contenedorAvatar.Controls.Add(this.pictureBox11);
            this.contenedorAvatar.Controls.Add(this.pictureBox12);
            this.contenedorAvatar.Controls.Add(this.pictureBox13);
            this.contenedorAvatar.Controls.Add(this.pictureBox14);
            this.contenedorAvatar.Controls.Add(this.pictureBox15);
            this.contenedorAvatar.Controls.Add(this.pictureBox16);
            this.contenedorAvatar.Location = new System.Drawing.Point(429, 117);
            this.contenedorAvatar.Name = "contenedorAvatar";
            this.contenedorAvatar.Padding = new System.Windows.Forms.Padding(5);
            this.contenedorAvatar.Size = new System.Drawing.Size(206, 131);
            this.contenedorAvatar.TabIndex = 6;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Image = global::Problema1.Properties.Resources.a1;
            this.pictureBox1.Location = new System.Drawing.Point(8, 8);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(33, 33);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.AvatarClick);
            this.pictureBox1.MouseEnter += new System.EventHandler(this.AvatarEntra);
            this.pictureBox1.MouseLeave += new System.EventHandler(this.AvatarSale);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Image = global::Problema1.Properties.Resources.a2;
            this.pictureBox2.Location = new System.Drawing.Point(47, 8);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(33, 33);
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.AvatarClick);
            this.pictureBox2.MouseEnter += new System.EventHandler(this.AvatarEntra);
            this.pictureBox2.MouseLeave += new System.EventHandler(this.AvatarSale);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox3.Image = global::Problema1.Properties.Resources.a3;
            this.pictureBox3.Location = new System.Drawing.Point(86, 8);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(33, 33);
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.MouseClick += new System.Windows.Forms.MouseEventHandler(this.AvatarClick);
            this.pictureBox3.MouseEnter += new System.EventHandler(this.AvatarEntra);
            this.pictureBox3.MouseLeave += new System.EventHandler(this.AvatarSale);
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox4.Image = global::Problema1.Properties.Resources.a4;
            this.pictureBox4.Location = new System.Drawing.Point(125, 8);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(33, 33);
            this.pictureBox4.TabIndex = 3;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.MouseClick += new System.Windows.Forms.MouseEventHandler(this.AvatarClick);
            this.pictureBox4.MouseEnter += new System.EventHandler(this.AvatarEntra);
            this.pictureBox4.MouseLeave += new System.EventHandler(this.AvatarSale);
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox5.Image = global::Problema1.Properties.Resources.a5;
            this.pictureBox5.Location = new System.Drawing.Point(8, 47);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(33, 33);
            this.pictureBox5.TabIndex = 4;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.MouseClick += new System.Windows.Forms.MouseEventHandler(this.AvatarClick);
            this.pictureBox5.MouseEnter += new System.EventHandler(this.AvatarEntra);
            this.pictureBox5.MouseLeave += new System.EventHandler(this.AvatarSale);
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox6.Image = global::Problema1.Properties.Resources.a6;
            this.pictureBox6.Location = new System.Drawing.Point(47, 47);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(33, 33);
            this.pictureBox6.TabIndex = 5;
            this.pictureBox6.TabStop = false;
            this.pictureBox6.MouseClick += new System.Windows.Forms.MouseEventHandler(this.AvatarClick);
            this.pictureBox6.MouseEnter += new System.EventHandler(this.AvatarEntra);
            this.pictureBox6.MouseLeave += new System.EventHandler(this.AvatarSale);
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox7.Image = global::Problema1.Properties.Resources.a7;
            this.pictureBox7.Location = new System.Drawing.Point(86, 47);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(33, 33);
            this.pictureBox7.TabIndex = 6;
            this.pictureBox7.TabStop = false;
            this.pictureBox7.MouseClick += new System.Windows.Forms.MouseEventHandler(this.AvatarClick);
            this.pictureBox7.MouseEnter += new System.EventHandler(this.AvatarEntra);
            this.pictureBox7.MouseLeave += new System.EventHandler(this.AvatarSale);
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox8.Image = global::Problema1.Properties.Resources.a8;
            this.pictureBox8.Location = new System.Drawing.Point(125, 47);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(33, 33);
            this.pictureBox8.TabIndex = 7;
            this.pictureBox8.TabStop = false;
            this.pictureBox8.MouseClick += new System.Windows.Forms.MouseEventHandler(this.AvatarClick);
            this.pictureBox8.MouseEnter += new System.EventHandler(this.AvatarEntra);
            this.pictureBox8.MouseLeave += new System.EventHandler(this.AvatarSale);
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox9.Image = global::Problema1.Properties.Resources.a9;
            this.pictureBox9.Location = new System.Drawing.Point(8, 86);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(33, 33);
            this.pictureBox9.TabIndex = 8;
            this.pictureBox9.TabStop = false;
            this.pictureBox9.MouseClick += new System.Windows.Forms.MouseEventHandler(this.AvatarClick);
            this.pictureBox9.MouseEnter += new System.EventHandler(this.AvatarEntra);
            this.pictureBox9.MouseLeave += new System.EventHandler(this.AvatarSale);
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox10.Image = global::Problema1.Properties.Resources.a10;
            this.pictureBox10.Location = new System.Drawing.Point(47, 86);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(33, 33);
            this.pictureBox10.TabIndex = 9;
            this.pictureBox10.TabStop = false;
            this.pictureBox10.MouseClick += new System.Windows.Forms.MouseEventHandler(this.AvatarClick);
            this.pictureBox10.MouseEnter += new System.EventHandler(this.AvatarEntra);
            this.pictureBox10.MouseLeave += new System.EventHandler(this.AvatarSale);
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox11.Image = global::Problema1.Properties.Resources.a11;
            this.pictureBox11.Location = new System.Drawing.Point(86, 86);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(33, 33);
            this.pictureBox11.TabIndex = 10;
            this.pictureBox11.TabStop = false;
            this.pictureBox11.MouseClick += new System.Windows.Forms.MouseEventHandler(this.AvatarClick);
            this.pictureBox11.MouseEnter += new System.EventHandler(this.AvatarEntra);
            this.pictureBox11.MouseLeave += new System.EventHandler(this.AvatarSale);
            // 
            // pictureBox12
            // 
            this.pictureBox12.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox12.Image = global::Problema1.Properties.Resources.a12;
            this.pictureBox12.Location = new System.Drawing.Point(125, 86);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(33, 33);
            this.pictureBox12.TabIndex = 11;
            this.pictureBox12.TabStop = false;
            this.pictureBox12.MouseClick += new System.Windows.Forms.MouseEventHandler(this.AvatarClick);
            this.pictureBox12.MouseEnter += new System.EventHandler(this.AvatarEntra);
            this.pictureBox12.MouseLeave += new System.EventHandler(this.AvatarSale);
            // 
            // pictureBox13
            // 
            this.pictureBox13.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox13.Image = global::Problema1.Properties.Resources.a13;
            this.pictureBox13.Location = new System.Drawing.Point(8, 125);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(33, 33);
            this.pictureBox13.TabIndex = 12;
            this.pictureBox13.TabStop = false;
            this.pictureBox13.MouseClick += new System.Windows.Forms.MouseEventHandler(this.AvatarClick);
            this.pictureBox13.MouseEnter += new System.EventHandler(this.AvatarEntra);
            this.pictureBox13.MouseLeave += new System.EventHandler(this.AvatarSale);
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox14.Image = global::Problema1.Properties.Resources.a14;
            this.pictureBox14.Location = new System.Drawing.Point(47, 125);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(33, 33);
            this.pictureBox14.TabIndex = 13;
            this.pictureBox14.TabStop = false;
            this.pictureBox14.MouseClick += new System.Windows.Forms.MouseEventHandler(this.AvatarClick);
            this.pictureBox14.MouseEnter += new System.EventHandler(this.AvatarEntra);
            this.pictureBox14.MouseLeave += new System.EventHandler(this.AvatarSale);
            // 
            // pictureBox15
            // 
            this.pictureBox15.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox15.Image = global::Problema1.Properties.Resources.a15;
            this.pictureBox15.Location = new System.Drawing.Point(86, 125);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(33, 33);
            this.pictureBox15.TabIndex = 14;
            this.pictureBox15.TabStop = false;
            this.pictureBox15.MouseClick += new System.Windows.Forms.MouseEventHandler(this.AvatarClick);
            this.pictureBox15.MouseEnter += new System.EventHandler(this.AvatarEntra);
            this.pictureBox15.MouseLeave += new System.EventHandler(this.AvatarSale);
            // 
            // pictureBox16
            // 
            this.pictureBox16.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox16.Image = global::Problema1.Properties.Resources.a16;
            this.pictureBox16.Location = new System.Drawing.Point(125, 125);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(33, 33);
            this.pictureBox16.TabIndex = 15;
            this.pictureBox16.TabStop = false;
            this.pictureBox16.MouseClick += new System.Windows.Forms.MouseEventHandler(this.AvatarClick);
            this.pictureBox16.MouseEnter += new System.EventHandler(this.AvatarEntra);
            this.pictureBox16.MouseLeave += new System.EventHandler(this.AvatarSale);
            // 
            // PictSelec
            // 
            this.PictSelec.BackColor = System.Drawing.Color.Transparent;
            this.PictSelec.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PictSelec.Image = global::Problema1.Properties.Resources.interrogante;
            this.PictSelec.Location = new System.Drawing.Point(39, 173);
            this.PictSelec.Name = "PictSelec";
            this.PictSelec.Size = new System.Drawing.Size(33, 33);
            this.PictSelec.TabIndex = 16;
            this.PictSelec.TabStop = false;
            this.PictSelec.Tag = "interrogante";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.DarkRed;
            this.label1.Location = new System.Drawing.Point(3, 125);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 42);
            this.label1.TabIndex = 17;
            this.label1.Text = "Avatar\nseleccionado:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label2.Location = new System.Drawing.Point(2, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 25);
            this.label2.TabIndex = 18;
            this.label2.Text = "Puntuación:";
            // 
            // lPuntuacion
            // 
            this.lPuntuacion.AutoSize = true;
            this.lPuntuacion.BackColor = System.Drawing.Color.Transparent;
            this.lPuntuacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lPuntuacion.ForeColor = System.Drawing.Color.Blue;
            this.lPuntuacion.Location = new System.Drawing.Point(29, 53);
            this.lPuntuacion.Name = "lPuntuacion";
            this.lPuntuacion.Size = new System.Drawing.Size(54, 24);
            this.lPuntuacion.TabIndex = 19;
            this.lPuntuacion.Text = "1000";
            // 
            // picLetra1
            // 
            this.picLetra1.BackColor = System.Drawing.Color.Transparent;
            this.picLetra1.Image = global::Problema1.Properties.Resources.l1;
            this.picLetra1.Location = new System.Drawing.Point(121, 147);
            this.picLetra1.Name = "picLetra1";
            this.picLetra1.Size = new System.Drawing.Size(57, 77);
            this.picLetra1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picLetra1.TabIndex = 16;
            this.picLetra1.TabStop = false;
            // 
            // picLetra2
            // 
            this.picLetra2.BackColor = System.Drawing.Color.Transparent;
            this.picLetra2.Image = global::Problema1.Properties.Resources.l1;
            this.picLetra2.Location = new System.Drawing.Point(210, 147);
            this.picLetra2.Name = "picLetra2";
            this.picLetra2.Size = new System.Drawing.Size(57, 77);
            this.picLetra2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picLetra2.TabIndex = 20;
            this.picLetra2.TabStop = false;
            // 
            // picLetra3
            // 
            this.picLetra3.BackColor = System.Drawing.Color.Transparent;
            this.picLetra3.Image = global::Problema1.Properties.Resources.l1;
            this.picLetra3.Location = new System.Drawing.Point(301, 147);
            this.picLetra3.Name = "picLetra3";
            this.picLetra3.Size = new System.Drawing.Size(57, 77);
            this.picLetra3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picLetra3.TabIndex = 21;
            this.picLetra3.TabStop = false;
            // 
            // aumenta1
            // 
            this.aumenta1.BackColor = System.Drawing.Color.Transparent;
            this.aumenta1.Location = new System.Drawing.Point(116, 108);
            this.aumenta1.Name = "aumenta1";
            this.aumenta1.Size = new System.Drawing.Size(62, 33);
            this.aumenta1.TabIndex = 22;
            this.aumenta1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.AumentaLetra);
            // 
            // aumenta2
            // 
            this.aumenta2.BackColor = System.Drawing.Color.Transparent;
            this.aumenta2.Location = new System.Drawing.Point(205, 108);
            this.aumenta2.Name = "aumenta2";
            this.aumenta2.Size = new System.Drawing.Size(62, 33);
            this.aumenta2.TabIndex = 23;
            this.aumenta2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.AumentaLetra);
            // 
            // aumenta3
            // 
            this.aumenta3.BackColor = System.Drawing.Color.Transparent;
            this.aumenta3.Location = new System.Drawing.Point(296, 108);
            this.aumenta3.Name = "aumenta3";
            this.aumenta3.Size = new System.Drawing.Size(62, 33);
            this.aumenta3.TabIndex = 24;
            this.aumenta3.MouseClick += new System.Windows.Forms.MouseEventHandler(this.AumentaLetra);
            // 
            // baja1
            // 
            this.baja1.BackColor = System.Drawing.Color.Transparent;
            this.baja1.Location = new System.Drawing.Point(116, 230);
            this.baja1.Name = "baja1";
            this.baja1.Size = new System.Drawing.Size(62, 33);
            this.baja1.TabIndex = 23;
            this.baja1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Disminuyeletra);
            // 
            // baja2
            // 
            this.baja2.BackColor = System.Drawing.Color.Transparent;
            this.baja2.Location = new System.Drawing.Point(205, 230);
            this.baja2.Name = "baja2";
            this.baja2.Size = new System.Drawing.Size(62, 33);
            this.baja2.TabIndex = 24;
            this.baja2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Disminuyeletra);
            // 
            // baja3
            // 
            this.baja3.BackColor = System.Drawing.Color.Transparent;
            this.baja3.Location = new System.Drawing.Point(296, 230);
            this.baja3.Name = "baja3";
            this.baja3.Size = new System.Drawing.Size(62, 33);
            this.baja3.TabIndex = 25;
            this.baja3.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Disminuyeletra);
            // 
            // botonGuardar
            // 
            this.botonGuardar.Location = new System.Drawing.Point(496, 28);
            this.botonGuardar.Name = "botonGuardar";
            this.botonGuardar.Size = new System.Drawing.Size(118, 41);
            this.botonGuardar.TabIndex = 26;
            this.botonGuardar.Text = "Guardar datos";
            this.botonGuardar.UseVisualStyleBackColor = true;
            this.botonGuardar.Click += new System.EventHandler(this.botonGuardar_Click);
            // 
            // Registro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Problema1.Properties.Resources.fondoReg;
            this.ClientSize = new System.Drawing.Size(647, 281);
            this.ControlBox = false;
            this.Controls.Add(this.botonGuardar);
            this.Controls.Add(this.baja3);
            this.Controls.Add(this.baja2);
            this.Controls.Add(this.baja1);
            this.Controls.Add(this.aumenta3);
            this.Controls.Add(this.aumenta2);
            this.Controls.Add(this.aumenta1);
            this.Controls.Add(this.picLetra3);
            this.Controls.Add(this.picLetra2);
            this.Controls.Add(this.lPuntuacion);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.contenedorAvatar);
            this.Controls.Add(this.PictSelec);
            this.Controls.Add(this.picLetra1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MinimizeBox = false;
            this.Name = "Registro";
            this.Text = "Registro";
            this.contenedorAvatar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictSelec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLetra1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLetra2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLetra3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.FlowLayoutPanel contenedorAvatar;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.PictureBox PictSelec;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lPuntuacion;
        private System.Windows.Forms.PictureBox picLetra1;
        private System.Windows.Forms.PictureBox picLetra2;
        private System.Windows.Forms.PictureBox picLetra3;
        private System.Windows.Forms.Panel aumenta1;
        private System.Windows.Forms.Panel aumenta2;
        private System.Windows.Forms.Panel aumenta3;
        private System.Windows.Forms.Panel baja1;
        private System.Windows.Forms.Panel baja2;
        private System.Windows.Forms.Panel baja3;
        private System.Windows.Forms.Button botonGuardar;
    }
}