﻿namespace Problema1
{
    partial class FormJugadores
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.bJug1 = new System.Windows.Forms.PictureBox();
            this.bJug2 = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bJug1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bJug2)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.bJug1);
            this.panel1.Controls.Add(this.bJug2);
            this.panel1.Location = new System.Drawing.Point(22, 25);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(249, 171);
            this.panel1.TabIndex = 0;
            // 
            // bJug1
            // 
            this.bJug1.Image = global::Problema1.Properties.Resources._1jugadorOFF;
            this.bJug1.Location = new System.Drawing.Point(14, 20);
            this.bJug1.Name = "bJug1";
            this.bJug1.Size = new System.Drawing.Size(217, 66);
            this.bJug1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bJug1.TabIndex = 0;
            this.bJug1.TabStop = false;
            this.bJug1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Accionar);
            this.bJug1.MouseEnter += new System.EventHandler(this.Seleccionado);
            this.bJug1.MouseLeave += new System.EventHandler(this.Deseleccionado);
            // 
            // bJug2
            // 
            this.bJug2.Image = global::Problema1.Properties.Resources._2jugadoresOFF;
            this.bJug2.Location = new System.Drawing.Point(14, 92);
            this.bJug2.Name = "bJug2";
            this.bJug2.Size = new System.Drawing.Size(217, 66);
            this.bJug2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bJug2.TabIndex = 1;
            this.bJug2.TabStop = false;
            this.bJug2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Accionar);
            this.bJug2.MouseEnter += new System.EventHandler(this.Seleccionado);
            this.bJug2.MouseLeave += new System.EventHandler(this.Deseleccionado);
            // 
            // FormJugadores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(303, 217);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FormJugadores";
            this.Text = "Selección de jugadores";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bJug1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bJug2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox bJug2;
        private System.Windows.Forms.PictureBox bJug1;
    }
}