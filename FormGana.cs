﻿using System;
using System.Windows.Forms;
using System.Media;

namespace Problema1
{
    public partial class FormGana : Form
    {
        SoundPlayer soundGana = new SoundPlayer(Properties.Resources.sonidoGana);

        /// <summary>
        /// Constructor base
        /// </summary>
        public FormGana()
        {
            InitializeComponent();
            soundGana.Play();
        }

        /// <summary>
        /// Cierra el formulario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormGana_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
