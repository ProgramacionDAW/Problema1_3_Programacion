﻿namespace Problema1
{
    class Juego
    {
        #region Atributos
        int puntuacion = 100;
        Tablero tablero;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor base
        /// </summary>
        public Juego()
        {
            tablero = new Tablero();
        }
        #endregion

        #region Propiedades
        /// <summary>
        /// Devuelve el tablero de juego
        /// </summary>
        public Tablero TableroJuego
        {
            get
            {
                return tablero;
            }
        }

        /// <summary>
        /// Devuelve la puntuación actual
        /// </summary>
        public int Puntuacion
        {
            get
            {
                return puntuacion;
            }
            set
            {
                puntuacion = value;
            }
        }
        #endregion
    }
}
