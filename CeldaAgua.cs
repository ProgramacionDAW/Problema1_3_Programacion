﻿namespace Problema1
{
    class CeldaAgua:Celda
    {
        #region Constructor
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public CeldaAgua()
        {

        }

        /// <summary>
        /// Constructor usado para generar una celda de tipo agua
        /// </summary>
        /// <param name="fila"></param>
        /// <param name="columna"></param>
        public CeldaAgua(int fila, int columna):base(fila,columna)
        {

        }
        #endregion

        #region Metodos
        /// <summary>
        /// Devuelve el estado de la celda
        /// </summary>
        /// <param name="formulario"></param>
        /// <param name="jugador"></param>
        /// <returns></returns>
        public override string Mostrar(Form1 formulario, int jugador)
        {
            estado = Estados.Agua;
            return Estado;
        }
        #endregion
    }
}
