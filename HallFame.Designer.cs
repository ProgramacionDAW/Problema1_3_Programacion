﻿namespace Problema1
{
    partial class HallFame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.imgAvatar1 = new System.Windows.Forms.PictureBox();
            this.imgAvatar2 = new System.Windows.Forms.PictureBox();
            this.imgAvatar3 = new System.Windows.Forms.PictureBox();
            this.imgAvatar4 = new System.Windows.Forms.PictureBox();
            this.imgAvatar5 = new System.Windows.Forms.PictureBox();
            this.imgAvatar6 = new System.Windows.Forms.PictureBox();
            this.imgAvatar7 = new System.Windows.Forms.PictureBox();
            this.imgAvatar8 = new System.Windows.Forms.PictureBox();
            this.imgAvatar9 = new System.Windows.Forms.PictureBox();
            this.imgAvatar10 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.pictureBox22 = new System.Windows.Forms.PictureBox();
            this.pictureBox23 = new System.Windows.Forms.PictureBox();
            this.labelN1 = new System.Windows.Forms.Label();
            this.labelN2 = new System.Windows.Forms.Label();
            this.labelN4 = new System.Windows.Forms.Label();
            this.labelN3 = new System.Windows.Forms.Label();
            this.labelN8 = new System.Windows.Forms.Label();
            this.labelN7 = new System.Windows.Forms.Label();
            this.labelN6 = new System.Windows.Forms.Label();
            this.labelN5 = new System.Windows.Forms.Label();
            this.labelN9 = new System.Windows.Forms.Label();
            this.labelN10 = new System.Windows.Forms.Label();
            this.labelP1 = new System.Windows.Forms.Label();
            this.labelP2 = new System.Windows.Forms.Label();
            this.labelP3 = new System.Windows.Forms.Label();
            this.labelP4 = new System.Windows.Forms.Label();
            this.labelP5 = new System.Windows.Forms.Label();
            this.labelP6 = new System.Windows.Forms.Label();
            this.labelP7 = new System.Windows.Forms.Label();
            this.labelP8 = new System.Windows.Forms.Label();
            this.labelP9 = new System.Windows.Forms.Label();
            this.labelP10 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.imgAvatar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAvatar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAvatar3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAvatar4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAvatar5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAvatar6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAvatar7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAvatar8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAvatar9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAvatar10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).BeginInit();
            this.SuspendLayout();
            // 
            // imgAvatar1
            // 
            this.imgAvatar1.Image = global::Problema1.Properties.Resources.interrogante;
            this.imgAvatar1.Location = new System.Drawing.Point(96, 208);
            this.imgAvatar1.Name = "imgAvatar1";
            this.imgAvatar1.Size = new System.Drawing.Size(34, 35);
            this.imgAvatar1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.imgAvatar1.TabIndex = 0;
            this.imgAvatar1.TabStop = false;
            // 
            // imgAvatar2
            // 
            this.imgAvatar2.Image = global::Problema1.Properties.Resources.interrogante;
            this.imgAvatar2.Location = new System.Drawing.Point(96, 249);
            this.imgAvatar2.Name = "imgAvatar2";
            this.imgAvatar2.Size = new System.Drawing.Size(34, 35);
            this.imgAvatar2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.imgAvatar2.TabIndex = 1;
            this.imgAvatar2.TabStop = false;
            // 
            // imgAvatar3
            // 
            this.imgAvatar3.Image = global::Problema1.Properties.Resources.interrogante;
            this.imgAvatar3.Location = new System.Drawing.Point(96, 290);
            this.imgAvatar3.Name = "imgAvatar3";
            this.imgAvatar3.Size = new System.Drawing.Size(34, 35);
            this.imgAvatar3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.imgAvatar3.TabIndex = 2;
            this.imgAvatar3.TabStop = false;
            // 
            // imgAvatar4
            // 
            this.imgAvatar4.Image = global::Problema1.Properties.Resources.interrogante;
            this.imgAvatar4.Location = new System.Drawing.Point(96, 331);
            this.imgAvatar4.Name = "imgAvatar4";
            this.imgAvatar4.Size = new System.Drawing.Size(34, 35);
            this.imgAvatar4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.imgAvatar4.TabIndex = 3;
            this.imgAvatar4.TabStop = false;
            // 
            // imgAvatar5
            // 
            this.imgAvatar5.Image = global::Problema1.Properties.Resources.interrogante;
            this.imgAvatar5.Location = new System.Drawing.Point(96, 372);
            this.imgAvatar5.Name = "imgAvatar5";
            this.imgAvatar5.Size = new System.Drawing.Size(34, 35);
            this.imgAvatar5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.imgAvatar5.TabIndex = 4;
            this.imgAvatar5.TabStop = false;
            // 
            // imgAvatar6
            // 
            this.imgAvatar6.Image = global::Problema1.Properties.Resources.interrogante;
            this.imgAvatar6.Location = new System.Drawing.Point(96, 413);
            this.imgAvatar6.Name = "imgAvatar6";
            this.imgAvatar6.Size = new System.Drawing.Size(34, 35);
            this.imgAvatar6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.imgAvatar6.TabIndex = 5;
            this.imgAvatar6.TabStop = false;
            // 
            // imgAvatar7
            // 
            this.imgAvatar7.Image = global::Problema1.Properties.Resources.interrogante;
            this.imgAvatar7.Location = new System.Drawing.Point(96, 454);
            this.imgAvatar7.Name = "imgAvatar7";
            this.imgAvatar7.Size = new System.Drawing.Size(34, 35);
            this.imgAvatar7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.imgAvatar7.TabIndex = 6;
            this.imgAvatar7.TabStop = false;
            // 
            // imgAvatar8
            // 
            this.imgAvatar8.Image = global::Problema1.Properties.Resources.interrogante;
            this.imgAvatar8.Location = new System.Drawing.Point(96, 495);
            this.imgAvatar8.Name = "imgAvatar8";
            this.imgAvatar8.Size = new System.Drawing.Size(34, 35);
            this.imgAvatar8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.imgAvatar8.TabIndex = 7;
            this.imgAvatar8.TabStop = false;
            // 
            // imgAvatar9
            // 
            this.imgAvatar9.Image = global::Problema1.Properties.Resources.interrogante;
            this.imgAvatar9.Location = new System.Drawing.Point(96, 536);
            this.imgAvatar9.Name = "imgAvatar9";
            this.imgAvatar9.Size = new System.Drawing.Size(34, 35);
            this.imgAvatar9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.imgAvatar9.TabIndex = 8;
            this.imgAvatar9.TabStop = false;
            // 
            // imgAvatar10
            // 
            this.imgAvatar10.Image = global::Problema1.Properties.Resources.interrogante;
            this.imgAvatar10.Location = new System.Drawing.Point(96, 577);
            this.imgAvatar10.Name = "imgAvatar10";
            this.imgAvatar10.Size = new System.Drawing.Size(34, 35);
            this.imgAvatar10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.imgAvatar10.TabIndex = 9;
            this.imgAvatar10.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = global::Problema1.Properties.Resources.puntos;
            this.pictureBox11.Location = new System.Drawing.Point(298, 225);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(262, 18);
            this.pictureBox11.TabIndex = 10;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.Image = global::Problema1.Properties.Resources.puntos;
            this.pictureBox12.Location = new System.Drawing.Point(298, 266);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(262, 18);
            this.pictureBox12.TabIndex = 11;
            this.pictureBox12.TabStop = false;
            // 
            // pictureBox13
            // 
            this.pictureBox13.Image = global::Problema1.Properties.Resources.puntos;
            this.pictureBox13.Location = new System.Drawing.Point(298, 307);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(262, 18);
            this.pictureBox13.TabIndex = 12;
            this.pictureBox13.TabStop = false;
            // 
            // pictureBox14
            // 
            this.pictureBox14.Image = global::Problema1.Properties.Resources.puntos;
            this.pictureBox14.Location = new System.Drawing.Point(298, 348);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(262, 18);
            this.pictureBox14.TabIndex = 13;
            this.pictureBox14.TabStop = false;
            // 
            // pictureBox15
            // 
            this.pictureBox15.Image = global::Problema1.Properties.Resources.puntos;
            this.pictureBox15.Location = new System.Drawing.Point(298, 389);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(262, 18);
            this.pictureBox15.TabIndex = 14;
            this.pictureBox15.TabStop = false;
            // 
            // pictureBox16
            // 
            this.pictureBox16.Image = global::Problema1.Properties.Resources.puntos;
            this.pictureBox16.Location = new System.Drawing.Point(298, 430);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(262, 18);
            this.pictureBox16.TabIndex = 15;
            this.pictureBox16.TabStop = false;
            // 
            // pictureBox17
            // 
            this.pictureBox17.Image = global::Problema1.Properties.Resources.puntos;
            this.pictureBox17.Location = new System.Drawing.Point(298, 471);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(262, 18);
            this.pictureBox17.TabIndex = 16;
            this.pictureBox17.TabStop = false;
            // 
            // pictureBox18
            // 
            this.pictureBox18.Image = global::Problema1.Properties.Resources.puntos;
            this.pictureBox18.Location = new System.Drawing.Point(298, 512);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(262, 18);
            this.pictureBox18.TabIndex = 17;
            this.pictureBox18.TabStop = false;
            // 
            // pictureBox19
            // 
            this.pictureBox19.Image = global::Problema1.Properties.Resources.puntos;
            this.pictureBox19.Location = new System.Drawing.Point(298, 553);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(262, 18);
            this.pictureBox19.TabIndex = 18;
            this.pictureBox19.TabStop = false;
            // 
            // pictureBox20
            // 
            this.pictureBox20.Image = global::Problema1.Properties.Resources.puntos;
            this.pictureBox20.Location = new System.Drawing.Point(298, 594);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(262, 18);
            this.pictureBox20.TabIndex = 19;
            this.pictureBox20.TabStop = false;
            // 
            // pictureBox21
            // 
            this.pictureBox21.Image = global::Problema1.Properties.Resources.trofeoOro;
            this.pictureBox21.Location = new System.Drawing.Point(261, 208);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(31, 35);
            this.pictureBox21.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox21.TabIndex = 20;
            this.pictureBox21.TabStop = false;
            // 
            // pictureBox22
            // 
            this.pictureBox22.Image = global::Problema1.Properties.Resources.trofeoPlata;
            this.pictureBox22.Location = new System.Drawing.Point(261, 249);
            this.pictureBox22.Name = "pictureBox22";
            this.pictureBox22.Size = new System.Drawing.Size(31, 35);
            this.pictureBox22.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox22.TabIndex = 21;
            this.pictureBox22.TabStop = false;
            // 
            // pictureBox23
            // 
            this.pictureBox23.Image = global::Problema1.Properties.Resources.trofeoBronce;
            this.pictureBox23.Location = new System.Drawing.Point(261, 290);
            this.pictureBox23.Name = "pictureBox23";
            this.pictureBox23.Size = new System.Drawing.Size(31, 35);
            this.pictureBox23.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox23.TabIndex = 22;
            this.pictureBox23.TabStop = false;
            // 
            // labelN1
            // 
            this.labelN1.AutoSize = true;
            this.labelN1.Font = new System.Drawing.Font("Consolas", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelN1.ForeColor = System.Drawing.Color.White;
            this.labelN1.Location = new System.Drawing.Point(156, 202);
            this.labelN1.Name = "labelN1";
            this.labelN1.Size = new System.Drawing.Size(79, 43);
            this.labelN1.TabIndex = 23;
            this.labelN1.Text = "AAA";
            // 
            // labelN2
            // 
            this.labelN2.AutoSize = true;
            this.labelN2.Font = new System.Drawing.Font("Consolas", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelN2.ForeColor = System.Drawing.Color.White;
            this.labelN2.Location = new System.Drawing.Point(156, 243);
            this.labelN2.Name = "labelN2";
            this.labelN2.Size = new System.Drawing.Size(79, 43);
            this.labelN2.TabIndex = 24;
            this.labelN2.Text = "AAA";
            // 
            // labelN4
            // 
            this.labelN4.AutoSize = true;
            this.labelN4.Font = new System.Drawing.Font("Consolas", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelN4.ForeColor = System.Drawing.Color.White;
            this.labelN4.Location = new System.Drawing.Point(156, 325);
            this.labelN4.Name = "labelN4";
            this.labelN4.Size = new System.Drawing.Size(79, 43);
            this.labelN4.TabIndex = 26;
            this.labelN4.Text = "AAA";
            // 
            // labelN3
            // 
            this.labelN3.AutoSize = true;
            this.labelN3.Font = new System.Drawing.Font("Consolas", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelN3.ForeColor = System.Drawing.Color.White;
            this.labelN3.Location = new System.Drawing.Point(156, 284);
            this.labelN3.Name = "labelN3";
            this.labelN3.Size = new System.Drawing.Size(79, 43);
            this.labelN3.TabIndex = 25;
            this.labelN3.Text = "AAA";
            // 
            // labelN8
            // 
            this.labelN8.AutoSize = true;
            this.labelN8.Font = new System.Drawing.Font("Consolas", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelN8.ForeColor = System.Drawing.Color.White;
            this.labelN8.Location = new System.Drawing.Point(156, 489);
            this.labelN8.Name = "labelN8";
            this.labelN8.Size = new System.Drawing.Size(79, 43);
            this.labelN8.TabIndex = 30;
            this.labelN8.Text = "AAA";
            // 
            // labelN7
            // 
            this.labelN7.AutoSize = true;
            this.labelN7.Font = new System.Drawing.Font("Consolas", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelN7.ForeColor = System.Drawing.Color.White;
            this.labelN7.Location = new System.Drawing.Point(156, 448);
            this.labelN7.Name = "labelN7";
            this.labelN7.Size = new System.Drawing.Size(79, 43);
            this.labelN7.TabIndex = 29;
            this.labelN7.Text = "AAA";
            // 
            // labelN6
            // 
            this.labelN6.AutoSize = true;
            this.labelN6.Font = new System.Drawing.Font("Consolas", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelN6.ForeColor = System.Drawing.Color.White;
            this.labelN6.Location = new System.Drawing.Point(156, 407);
            this.labelN6.Name = "labelN6";
            this.labelN6.Size = new System.Drawing.Size(79, 43);
            this.labelN6.TabIndex = 28;
            this.labelN6.Text = "AAA";
            // 
            // labelN5
            // 
            this.labelN5.AutoSize = true;
            this.labelN5.Font = new System.Drawing.Font("Consolas", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelN5.ForeColor = System.Drawing.Color.White;
            this.labelN5.Location = new System.Drawing.Point(156, 366);
            this.labelN5.Name = "labelN5";
            this.labelN5.Size = new System.Drawing.Size(79, 43);
            this.labelN5.TabIndex = 27;
            this.labelN5.Text = "AAA";
            // 
            // labelN9
            // 
            this.labelN9.AutoSize = true;
            this.labelN9.Font = new System.Drawing.Font("Consolas", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelN9.ForeColor = System.Drawing.Color.White;
            this.labelN9.Location = new System.Drawing.Point(156, 530);
            this.labelN9.Name = "labelN9";
            this.labelN9.Size = new System.Drawing.Size(79, 43);
            this.labelN9.TabIndex = 31;
            this.labelN9.Text = "AAA";
            // 
            // labelN10
            // 
            this.labelN10.AutoSize = true;
            this.labelN10.Font = new System.Drawing.Font("Consolas", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelN10.ForeColor = System.Drawing.Color.White;
            this.labelN10.Location = new System.Drawing.Point(156, 569);
            this.labelN10.Name = "labelN10";
            this.labelN10.Size = new System.Drawing.Size(79, 43);
            this.labelN10.TabIndex = 32;
            this.labelN10.Text = "AAA";
            // 
            // labelP1
            // 
            this.labelP1.AutoSize = true;
            this.labelP1.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelP1.ForeColor = System.Drawing.Color.White;
            this.labelP1.Location = new System.Drawing.Point(566, 213);
            this.labelP1.Name = "labelP1";
            this.labelP1.Size = new System.Drawing.Size(75, 32);
            this.labelP1.TabIndex = 33;
            this.labelP1.Text = "1000";
            // 
            // labelP2
            // 
            this.labelP2.AutoSize = true;
            this.labelP2.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelP2.ForeColor = System.Drawing.Color.White;
            this.labelP2.Location = new System.Drawing.Point(566, 254);
            this.labelP2.Name = "labelP2";
            this.labelP2.Size = new System.Drawing.Size(75, 32);
            this.labelP2.TabIndex = 34;
            this.labelP2.Text = "1000";
            // 
            // labelP3
            // 
            this.labelP3.AutoSize = true;
            this.labelP3.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelP3.ForeColor = System.Drawing.Color.White;
            this.labelP3.Location = new System.Drawing.Point(566, 295);
            this.labelP3.Name = "labelP3";
            this.labelP3.Size = new System.Drawing.Size(75, 32);
            this.labelP3.TabIndex = 35;
            this.labelP3.Text = "1000";
            // 
            // labelP4
            // 
            this.labelP4.AutoSize = true;
            this.labelP4.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelP4.ForeColor = System.Drawing.Color.White;
            this.labelP4.Location = new System.Drawing.Point(566, 336);
            this.labelP4.Name = "labelP4";
            this.labelP4.Size = new System.Drawing.Size(75, 32);
            this.labelP4.TabIndex = 36;
            this.labelP4.Text = "1000";
            // 
            // labelP5
            // 
            this.labelP5.AutoSize = true;
            this.labelP5.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelP5.ForeColor = System.Drawing.Color.White;
            this.labelP5.Location = new System.Drawing.Point(566, 375);
            this.labelP5.Name = "labelP5";
            this.labelP5.Size = new System.Drawing.Size(75, 32);
            this.labelP5.TabIndex = 37;
            this.labelP5.Text = "1000";
            // 
            // labelP6
            // 
            this.labelP6.AutoSize = true;
            this.labelP6.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelP6.ForeColor = System.Drawing.Color.White;
            this.labelP6.Location = new System.Drawing.Point(566, 418);
            this.labelP6.Name = "labelP6";
            this.labelP6.Size = new System.Drawing.Size(75, 32);
            this.labelP6.TabIndex = 38;
            this.labelP6.Text = "1000";
            // 
            // labelP7
            // 
            this.labelP7.AutoSize = true;
            this.labelP7.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelP7.ForeColor = System.Drawing.Color.White;
            this.labelP7.Location = new System.Drawing.Point(566, 457);
            this.labelP7.Name = "labelP7";
            this.labelP7.Size = new System.Drawing.Size(75, 32);
            this.labelP7.TabIndex = 39;
            this.labelP7.Text = "1000";
            // 
            // labelP8
            // 
            this.labelP8.AutoSize = true;
            this.labelP8.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelP8.ForeColor = System.Drawing.Color.White;
            this.labelP8.Location = new System.Drawing.Point(566, 500);
            this.labelP8.Name = "labelP8";
            this.labelP8.Size = new System.Drawing.Size(75, 32);
            this.labelP8.TabIndex = 40;
            this.labelP8.Text = "1000";
            // 
            // labelP9
            // 
            this.labelP9.AutoSize = true;
            this.labelP9.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelP9.ForeColor = System.Drawing.Color.White;
            this.labelP9.Location = new System.Drawing.Point(566, 541);
            this.labelP9.Name = "labelP9";
            this.labelP9.Size = new System.Drawing.Size(75, 32);
            this.labelP9.TabIndex = 41;
            this.labelP9.Text = "1000";
            // 
            // labelP10
            // 
            this.labelP10.AutoSize = true;
            this.labelP10.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelP10.ForeColor = System.Drawing.Color.White;
            this.labelP10.Location = new System.Drawing.Point(566, 582);
            this.labelP10.Name = "labelP10";
            this.labelP10.Size = new System.Drawing.Size(75, 32);
            this.labelP10.TabIndex = 42;
            this.labelP10.Text = "1000";
            // 
            // HallFame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.BackgroundImage = global::Problema1.Properties.Resources.hallFondo;
            this.ClientSize = new System.Drawing.Size(688, 632);
            this.Controls.Add(this.labelP10);
            this.Controls.Add(this.labelP9);
            this.Controls.Add(this.labelP8);
            this.Controls.Add(this.labelP7);
            this.Controls.Add(this.labelP6);
            this.Controls.Add(this.labelP5);
            this.Controls.Add(this.labelP4);
            this.Controls.Add(this.labelP3);
            this.Controls.Add(this.labelP2);
            this.Controls.Add(this.labelP1);
            this.Controls.Add(this.labelN10);
            this.Controls.Add(this.labelN9);
            this.Controls.Add(this.labelN8);
            this.Controls.Add(this.labelN7);
            this.Controls.Add(this.labelN6);
            this.Controls.Add(this.labelN5);
            this.Controls.Add(this.labelN4);
            this.Controls.Add(this.labelN3);
            this.Controls.Add(this.labelN2);
            this.Controls.Add(this.labelN1);
            this.Controls.Add(this.pictureBox23);
            this.Controls.Add(this.pictureBox22);
            this.Controls.Add(this.pictureBox21);
            this.Controls.Add(this.pictureBox20);
            this.Controls.Add(this.pictureBox19);
            this.Controls.Add(this.pictureBox18);
            this.Controls.Add(this.pictureBox17);
            this.Controls.Add(this.pictureBox16);
            this.Controls.Add(this.pictureBox15);
            this.Controls.Add(this.pictureBox14);
            this.Controls.Add(this.pictureBox13);
            this.Controls.Add(this.pictureBox12);
            this.Controls.Add(this.pictureBox11);
            this.Controls.Add(this.imgAvatar10);
            this.Controls.Add(this.imgAvatar9);
            this.Controls.Add(this.imgAvatar8);
            this.Controls.Add(this.imgAvatar7);
            this.Controls.Add(this.imgAvatar6);
            this.Controls.Add(this.imgAvatar5);
            this.Controls.Add(this.imgAvatar4);
            this.Controls.Add(this.imgAvatar3);
            this.Controls.Add(this.imgAvatar2);
            this.Controls.Add(this.imgAvatar1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "HallFame";
            this.Text = "Hall of Fame";
            ((System.ComponentModel.ISupportInitialize)(this.imgAvatar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAvatar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAvatar3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAvatar4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAvatar5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAvatar6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAvatar7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAvatar8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAvatar9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAvatar10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox imgAvatar1;
        private System.Windows.Forms.PictureBox imgAvatar2;
        private System.Windows.Forms.PictureBox imgAvatar3;
        private System.Windows.Forms.PictureBox imgAvatar4;
        private System.Windows.Forms.PictureBox imgAvatar5;
        private System.Windows.Forms.PictureBox imgAvatar6;
        private System.Windows.Forms.PictureBox imgAvatar7;
        private System.Windows.Forms.PictureBox imgAvatar8;
        private System.Windows.Forms.PictureBox imgAvatar9;
        private System.Windows.Forms.PictureBox imgAvatar10;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.PictureBox pictureBox21;
        private System.Windows.Forms.PictureBox pictureBox22;
        private System.Windows.Forms.PictureBox pictureBox23;
        private System.Windows.Forms.Label labelN1;
        private System.Windows.Forms.Label labelN2;
        private System.Windows.Forms.Label labelN4;
        private System.Windows.Forms.Label labelN3;
        private System.Windows.Forms.Label labelN8;
        private System.Windows.Forms.Label labelN7;
        private System.Windows.Forms.Label labelN6;
        private System.Windows.Forms.Label labelN5;
        private System.Windows.Forms.Label labelN9;
        private System.Windows.Forms.Label labelN10;
        private System.Windows.Forms.Label labelP1;
        private System.Windows.Forms.Label labelP2;
        private System.Windows.Forms.Label labelP3;
        private System.Windows.Forms.Label labelP4;
        private System.Windows.Forms.Label labelP5;
        private System.Windows.Forms.Label labelP6;
        private System.Windows.Forms.Label labelP7;
        private System.Windows.Forms.Label labelP8;
        private System.Windows.Forms.Label labelP9;
        private System.Windows.Forms.Label labelP10;
    }
}