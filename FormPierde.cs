﻿using System;
using System.Windows.Forms;
using System.Media;

namespace Problema1
{
    public partial class FormPierde : Form
    {
        SoundPlayer soundPierde = new SoundPlayer(Properties.Resources.sonidoPierde);

        /// <summary>
        /// Constructor base
        /// </summary>
        public FormPierde()
        {
            InitializeComponent();
            soundPierde.Play();
        }

        /// <summary>
        /// Cierra el formulario al hacer click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormPierde_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
