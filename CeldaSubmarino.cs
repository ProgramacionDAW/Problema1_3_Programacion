﻿namespace Problema1
{
    class CeldaSubmarino:Celda
    {
        #region Constructor
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public CeldaSubmarino()
        {

        }

        /// <summary>
        /// Constructor usado para generar una celda de tipo submarino
        /// </summary>
        /// <param name="fila"></param>
        /// <param name="columna"></param>
        public CeldaSubmarino(int fila, int columna):base(fila,columna)
        {

        }
        #endregion

        #region Metodos
        /// <summary>
        /// Devuelve el estado de la celda
        /// </summary>
        /// <param name="formulario"></param>
        /// <param name="jugador"></param>
        /// <returns></returns>
        public override string Mostrar(Form1 formulario, int jugador)
        {
            estado = Estados.Hundido;
            string tipo = GetType().Name;
            formulario.ActualizarCantidadBarcos(tipo);
            return Estado;
        }
        #endregion
    }
}
