﻿using System;
using System.Collections.Generic;

namespace Problema1
{
    class Analizador
    {
        #region Atributos
        List<CeldaAgua> agua;
        int[,] submarinos;
        int[,] destructores;
        int[,] cruceros;
        int[,] acorazado;
        int indexSubmarino = 0;
        int indexDestructores = 0;
        int indexCruceros = 0;
        int indexAcorazado = 0;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor base
        /// </summary>
        public Analizador()
        {
            agua = new List<CeldaAgua>(100);
            LlenarCeldas();
            submarinos = new int[4, 2];
            destructores = new int[6, 2];
            cruceros = new int[6, 2];
            acorazado = new int[4, 2];
        }
        #endregion

        #region Metodos
        /// <summary>
        /// Llena el array de celdas de tipo Agua
        /// </summary>
        private void LlenarCeldas()
        {
            int fila = 0;
            int columna = 0;
            for (int i=0; i<100;i++)
            {
                agua.Add(new CeldaAgua(fila, columna));
                columna++;
                if(columna>=10)
                {
                    columna = 0;
                    fila++;
                }
            }
        }

        /// <summary>
        /// Genera las celdas requeridas para la aplicación
        /// </summary>
        /// <param name="espacio">Indica el tipo de barco que generará</param>
        /// <returns></returns>
        public int[,] ObtenerCeldas(int espacio)
        {
            int cantidad;
            if (espacio==1)
            {
                cantidad = 4;
                for (int i=0; i<cantidad;i++)
                {
                    GenerarCeldas(espacio);
                }
                return submarinos;
            }
            else if (espacio == 2)
            {
                cantidad = 3;
                for (int i = 0; i < cantidad; i++)
                {
                    GenerarCeldas(espacio);
                }
                return destructores;
            }
            else if (espacio==3)
            {
                cantidad = 2;
                for (int i = 0; i < cantidad; i++)
                {
                    GenerarCeldas(espacio);
                }
                return cruceros;
            }
            else
            {
                cantidad = 1;
                for (int i = 0; i < cantidad; i++)
                {
                    GenerarCeldas(espacio);
                }
                return acorazado;
            }
        }

        /// <summary>
        /// Inicia el proceso de obtención de celdas
        /// </summary>
        /// <param name="espacio">Numero de celdas que ocupará el barco</param>
        private void GenerarCeldas(int espacio)
        {
            int[,] posiblesCeldas;
            int margen = espacio - 1; //Se le resta la celda que ocuparia y obtenemos las celdas que necesitamos
            Random r = new Random();
            int fila;
            int columna;
            int direccion;
            bool vertical;
            bool avanzar=true;
            do
            {
                posiblesCeldas = new int[espacio, 2];
                //selección de direccion (vertical o horizontal)
                direccion = r.Next(0, 2);
                if (direccion == 0)
                {
                    vertical = true;
                }
                else
                {
                    vertical = false;
                }

                //genera los puntos
                if (vertical)
                {
                    fila = r.Next(0, 10 - margen);
                    columna = r.Next(0, 10);
                }
                else //horizontal
                {
                    fila = r.Next(0, 10);
                    columna = r.Next(0, 10 - margen);
                }
                posiblesCeldas[0, 0] = fila;
                posiblesCeldas[0, 1] = columna;
                for (int i = 1; i < espacio; i++)
                {
                    if (vertical)
                    {
                        posiblesCeldas[i, 0] = fila + i;
                        posiblesCeldas[i, 1] = columna;
                    }
                    else //horizontal
                    {
                        posiblesCeldas[i, 0] = fila;
                        posiblesCeldas[i, 1] = columna + i;
                    }
                }
                //comprobamos los puntos
                avanzar = ComprobarCeldas(posiblesCeldas);
                if (avanzar)
                {
                    //generamos los margenes
                    int[,] margenes = GenerarMargenes(fila, columna, vertical, espacio);

                    //los comprobamos
                    avanzar = ComprobarMargenes(margenes);

                    if (avanzar)
                    {
                        AgregaCeldas(posiblesCeldas, espacio);
                        for (int i=0;i<posiblesCeldas.GetLength(0);i++)
                        {
                            int filaEliminar = posiblesCeldas[i, 0];
                            int columnaEliminar = posiblesCeldas[i, 1];
                            EliminarCelda(filaEliminar, columnaEliminar);
                        }
                    }
                }
            } while (avanzar == false);
        }

        /// <summary>
        /// Genera los márgenes teniendo como referencia la primera celda y el espacio que ocupará
        /// </summary>
        /// <param name="fila"></param>
        /// <param name="columna"></param>
        /// <param name="vertical"></param>
        /// <param name="espacio"></param>
        /// <returns></returns>
        private int[,] GenerarMargenes(int fila, int columna, bool vertical, int espacio)
        {
            int[,] margenes;
            #region Submarinos
            if (espacio == 1)
            {
                margenes = new int[8, 2];
                for (int i = 0; i < margenes.GetLength(0); i++)
                {
                    switch (i)
                    {
                        case 0:
                            margenes[i, 0] = fila - 1;
                            margenes[i, 1] = columna;
                            break;
                        case 1:
                            margenes[i, 0] = fila + 1;
                            margenes[i, 1] = columna;
                            break;
                        case 2:
                            margenes[i, 0] = fila;
                            margenes[i, 1] = columna + 1;
                            break;
                        case 3:
                            margenes[i, 0] = fila;
                            margenes[i, 1] = columna - 1;
                            break;
                        case 4:
                            margenes[i, 0] = fila - 1;
                            margenes[i, 1] = columna - 1;
                            break;
                        case 5:
                            margenes[i, 0] = fila - 1;
                            margenes[i, 1] = columna + 1;
                            break;
                        case 6:
                            margenes[i, 0] = fila + 1;
                            margenes[i, 1] = columna + 1;
                            break;
                        case 7:
                            margenes[i, 0] = fila + 1;
                            margenes[i, 1] = columna - 1;
                            break;
                    }
                }
            }
            #endregion
            #region Destructores
            else if (espacio == 2)
            {
                margenes = new int[10, 2];
                if (vertical)
                {
                    for (int i = 0; i < margenes.GetLength(0); i++)
                    {
                        switch (i)
                        {
                            case 0:
                                margenes[i, 0] = fila - 1;
                                margenes[i, 1] = columna;
                                break;
                            case 1:
                                margenes[i, 0] = fila + 2;
                                margenes[i, 1] = columna;
                                break;
                            case 2:
                                margenes[i, 0] = fila;
                                margenes[i, 1] = columna + 1;
                                break;
                            case 3:
                                margenes[i, 0] = fila;
                                margenes[i, 1] = columna - 1;
                                break;
                            case 4:
                                margenes[i, 0] = fila - 1;
                                margenes[i, 1] = columna - 1;
                                break;
                            case 5:
                                margenes[i, 0] = fila - 1;
                                margenes[i, 1] = columna + 1;
                                break;
                            case 6:
                                margenes[i, 0] = fila + 1;
                                margenes[i, 1] = columna + 1;
                                break;
                            case 7:
                                margenes[i, 0] = fila + 1;
                                margenes[i, 1] = columna - 1;
                                break;
                            case 8:
                                margenes[i, 0] = fila + 2;
                                margenes[i, 1] = columna - 1;
                                break;
                            case 9:
                                margenes[i, 0] = fila + 2;
                                margenes[i, 1] = columna + 1;
                                break;
                        }
                    }
                }
                else //horizontal
                {
                    for (int i = 0; i < margenes.GetLength(0); i++)
                    {
                        switch (i)
                        {
                            case 0:
                                margenes[i, 0] = fila;
                                margenes[i, 1] = columna - 1;
                                break;
                            case 1:
                                margenes[i, 0] = fila;
                                margenes[i, 1] = columna + 2;
                                break;
                            case 2:
                                margenes[i, 0] = fila + 1;
                                margenes[i, 1] = columna;
                                break;
                            case 3:
                                margenes[i, 0] = fila - 1;
                                margenes[i, 1] = columna;
                                break;
                            case 4:
                                margenes[i, 0] = fila - 1;
                                margenes[i, 1] = columna - 1;
                                break;
                            case 5:
                                margenes[i, 0] = fila + 1;
                                margenes[i, 1] = columna - 1;
                                break;
                            case 6:
                                margenes[i, 0] = fila - 1;
                                margenes[i, 1] = columna + 1;
                                break;
                            case 7:
                                margenes[i, 0] = fila + 1;
                                margenes[i, 1] = columna + 1;
                                break;
                            case 8:
                                margenes[i, 0] = fila - 1;
                                margenes[i, 1] = columna + 2;
                                break;
                            case 9:
                                margenes[i, 0] = fila + 1;
                                margenes[i, 1] = columna + 2;
                                break;
                        }
                    }
                }
            }
            #endregion
            #region Cruceros
            else if (espacio == 3)
            {
                margenes = new int[12, 2];
                if (vertical)
                {
                    for (int i = 0; i < margenes.GetLength(0); i++)
                    {
                        switch (i)
                        {
                            case 0:
                                margenes[i, 0] = fila - 1;
                                margenes[i, 1] = columna;
                                break;
                            case 1:
                                margenes[i, 0] = fila + 3;
                                margenes[i, 1] = columna;
                                break;
                            case 2:
                                margenes[i, 0] = fila;
                                margenes[i, 1] = columna + 1;
                                break;
                            case 3:
                                margenes[i, 0] = fila;
                                margenes[i, 1] = columna - 1;
                                break;
                            case 4:
                                margenes[i, 0] = fila - 1;
                                margenes[i, 1] = columna - 1;
                                break;
                            case 5:
                                margenes[i, 0] = fila - 1;
                                margenes[i, 1] = columna + 1;
                                break;
                            case 6:
                                margenes[i, 0] = fila + 1;
                                margenes[i, 1] = columna + 1;
                                break;
                            case 7:
                                margenes[i, 0] = fila + 1;
                                margenes[i, 1] = columna - 1;
                                break;
                            case 8:
                                margenes[i, 0] = fila + 2;
                                margenes[i, 1] = columna - 1;
                                break;
                            case 9:
                                margenes[i, 0] = fila + 2;
                                margenes[i, 1] = columna + 1;
                                break;
                            case 10:
                                margenes[i, 0] = fila + 3;
                                margenes[i, 1] = columna + 1;
                                break;
                            case 11:
                                margenes[i, 0] = fila + 3;
                                margenes[i, 1] = columna - 1;
                                break;
                        }
                    }
                }
                else //horizontal
                {
                    for (int i = 0; i < margenes.GetLength(0); i++)
                    {
                        switch (i)
                        {
                            case 0:
                                margenes[i, 0] = fila;
                                margenes[i, 1] = columna - 1;
                                break;
                            case 1:
                                margenes[i, 0] = fila;
                                margenes[i, 1] = columna + 3;
                                break;
                            case 2:
                                margenes[i, 0] = fila + 1;
                                margenes[i, 1] = columna;
                                break;
                            case 3:
                                margenes[i, 0] = fila - 1;
                                margenes[i, 1] = columna;
                                break;
                            case 4:
                                margenes[i, 0] = fila - 1;
                                margenes[i, 1] = columna - 1;
                                break;
                            case 5:
                                margenes[i, 0] = fila + 1;
                                margenes[i, 1] = columna - 1;
                                break;
                            case 6:
                                margenes[i, 0] = fila - 1;
                                margenes[i, 1] = columna + 1;
                                break;
                            case 7:
                                margenes[i, 0] = fila + 1;
                                margenes[i, 1] = columna + 1;
                                break;
                            case 8:
                                margenes[i, 0] = fila - 1;
                                margenes[i, 1] = columna + 2;
                                break;
                            case 9:
                                margenes[i, 0] = fila + 1;
                                margenes[i, 1] = columna + 2;
                                break;
                            case 10:
                                margenes[i, 0] = fila + 1;
                                margenes[i, 1] = columna + 3;
                                break;
                            case 11:
                                margenes[i, 0] = fila - 1;
                                margenes[i, 1] = columna + 3;
                                break;
                        }
                    }
                }
            }
            #endregion
            #region Acorazados
            else
            {
                margenes = new int[14, 2];
                if (vertical)
                {
                    for (int i = 0; i < margenes.GetLength(0); i++)
                    {
                        switch (i)
                        {
                            case 0:
                                margenes[i, 0] = fila - 1;
                                margenes[i, 1] = columna;
                                break;
                            case 1:
                                margenes[i, 0] = fila + 4;
                                margenes[i, 1] = columna;
                                break;
                            case 2:
                                margenes[i, 0] = fila;
                                margenes[i, 1] = columna + 1;
                                break;
                            case 3:
                                margenes[i, 0] = fila;
                                margenes[i, 1] = columna - 1;
                                break;
                            case 4:
                                margenes[i, 0] = fila - 1;
                                margenes[i, 1] = columna - 1;
                                break;
                            case 5:
                                margenes[i, 0] = fila - 1;
                                margenes[i, 1] = columna + 1;
                                break;
                            case 6:
                                margenes[i, 0] = fila + 1;
                                margenes[i, 1] = columna + 1;
                                break;
                            case 7:
                                margenes[i, 0] = fila + 1;
                                margenes[i, 1] = columna - 1;
                                break;
                            case 8:
                                margenes[i, 0] = fila + 2;
                                margenes[i, 1] = columna - 1;
                                break;
                            case 9:
                                margenes[i, 0] = fila + 2;
                                margenes[i, 1] = columna + 1;
                                break;
                            case 10:
                                margenes[i, 0] = fila + 3;
                                margenes[i, 1] = columna + 1;
                                break;
                            case 11:
                                margenes[i, 0] = fila + 3;
                                margenes[i, 1] = columna - 1;
                                break;
                            case 12:
                                margenes[i, 0] = fila + 4;
                                margenes[i, 1] = columna - 1;
                                break;
                            case 13:
                                margenes[i, 0] = fila + 4;
                                margenes[i, 1] = columna + 1;
                                break;
                        }
                    }
                }
                else //horizontal
                {
                    for (int i = 0; i < margenes.GetLength(0); i++)
                    {
                        switch (i)
                        {
                            case 0:
                                margenes[i, 0] = fila;
                                margenes[i, 1] = columna - 1;
                                break;
                            case 1:
                                margenes[i, 0] = fila;
                                margenes[i, 1] = columna + 4;
                                break;
                            case 2:
                                margenes[i, 0] = fila + 1;
                                margenes[i, 1] = columna;
                                break;
                            case 3:
                                margenes[i, 0] = fila - 1;
                                margenes[i, 1] = columna;
                                break;
                            case 4:
                                margenes[i, 0] = fila - 1;
                                margenes[i, 1] = columna - 1;
                                break;
                            case 5:
                                margenes[i, 0] = fila + 1;
                                margenes[i, 1] = columna - 1;
                                break;
                            case 6:
                                margenes[i, 0] = fila - 1;
                                margenes[i, 1] = columna + 1;
                                break;
                            case 7:
                                margenes[i, 0] = fila + 1;
                                margenes[i, 1] = columna + 1;
                                break;
                            case 8:
                                margenes[i, 0] = fila - 1;
                                margenes[i, 1] = columna + 2;
                                break;
                            case 9:
                                margenes[i, 0] = fila + 1;
                                margenes[i, 1] = columna + 2;
                                break;
                            case 10:
                                margenes[i, 0] = fila + 1;
                                margenes[i, 1] = columna + 3;
                                break;
                            case 11:
                                margenes[i, 0] = fila - 1;
                                margenes[i, 1] = columna + 3;
                                break;
                            case 12:
                                margenes[i, 0] = fila - 1;
                                margenes[i, 1] = columna + 4;
                                break;
                            case 13:
                                margenes[i, 0] = fila + 1;
                                margenes[i, 1] = columna + 4;
                                break;
                        }
                    }
                }
            }
            #endregion
            return margenes;
        }

        /// <summary>
        /// Comprueba que las celdas generadas esten disponibles
        /// </summary>
        /// <param name="posCeldas">Contiene las celdas generadas</param>
        /// <returns></returns>
        private bool ComprobarCeldas(int[,] posCeldas)
        {
            bool encontrado = true;
            for(int i=0;i<posCeldas.GetLength(0);i++)
            {
                if (encontrado)
                {
                    int fila = posCeldas[i, 0];
                    int columna = posCeldas[i, 1];
                    encontrado = BuscarCeldas(fila, columna);
                }
                else
                {
                    return false;
                }
            }
            return encontrado;
        }

        /// <summary>
        /// Comprueba que los márgenes generados esten disponibles
        /// </summary>
        /// <param name="posCeldas"></param>
        /// <returns></returns>
        private bool ComprobarMargenes(int[,] posCeldas)
        {
            bool encontrado = true;
            for (int i = 0; i < posCeldas.GetLength(0); i++)
            {
                int fila = posCeldas[i, 0];
                int columna = posCeldas[i, 1];
                if ((fila>=0 && columna>=0) && (fila<10 && columna<10))
                {
                    if (encontrado)
                    {
                        encontrado = BuscarCeldas(fila, columna);
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            return encontrado;
        }

        /// <summary>
        /// Comprueba la existencia de una celda concreta
        /// </summary>
        /// <param name="fila"></param>
        /// <param name="columna"></param>
        /// <returns></returns>
        private bool BuscarCeldas(int fila, int columna)
        {
            for (int i = 0; i < agua.Count; i++)
            {
                int filaAgua = agua[i].Fila;
                int columnaAgua = agua[i].Columna;
                if (filaAgua == fila && columnaAgua == columna)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Agrega las celdas generadas a su array correspondiente dependiendo del espacio que ocupe
        /// </summary>
        /// <param name="posCeldas"></param>
        /// <param name="espacio"></param>
        private void AgregaCeldas(int[,] posCeldas, int espacio)
        {
            switch (espacio)
            {
                case 1:
                    for (int i = 0; i < posCeldas.GetLength(0); i++)
                    {
                        submarinos[indexSubmarino, 0] = posCeldas[i, 0];
                        submarinos[indexSubmarino, 1] = posCeldas[i, 1];
                        indexSubmarino++;
                    }
                    break;
                case 2:
                    for (int i = 0; i < posCeldas.GetLength(0); i++)
                    {
                        destructores[indexDestructores, 0] = posCeldas[i, 0];
                        destructores[indexDestructores, 1] = posCeldas[i, 1];
                        indexDestructores++;
                    }
                    break;
                case 3:
                    for (int i = 0; i < posCeldas.GetLength(0); i++)
                    {
                        cruceros[indexCruceros, 0] = posCeldas[i, 0];
                        cruceros[indexCruceros, 1] = posCeldas[i, 1];
                        indexCruceros++;
                    }
                    break;
                case 4:
                    for (int i = 0; i < posCeldas.GetLength(0); i++)
                    {
                        acorazado[indexAcorazado, 0] = posCeldas[i, 0];
                        acorazado[indexAcorazado, 1] = posCeldas[i, 1];
                        indexAcorazado++;
                    }
                    break;
            }
        }

        /// <summary>
        /// Elimina una celda del array
        /// </summary>
        /// <param name="fila"></param>
        /// <param name="columna"></param>
        private void EliminarCelda(int fila, int columna)
        {
            int index = EncontrarIndex(fila, columna);
            agua.RemoveAt(index);
        }

        /// <summary>
        /// Devuelve la posicion en el array de una celda concreta
        /// </summary>
        /// <param name="fila"></param>
        /// <param name="columna"></param>
        /// <returns></returns>
        private int EncontrarIndex(int fila, int columna)
        {
            foreach (CeldaAgua celda in agua)
            {
                if (celda.Fila==fila && celda.Columna==columna)
                {
                    return agua.IndexOf(celda);
                }
            }
            return -1; //En caso de que la celda no exista
        }

        #endregion
    }
}
