﻿using System.Collections.Generic;

namespace Problema1
{
    class Tablero
    {
        #region Atributos
        List<Celda> celdas;
        Analizador analizador;
        int submarinos = 4;
        int destructores = 3;
        int cruceros = 2;
        int acorazados = 1;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor base
        /// </summary>
        public Tablero()
        {
            celdas = new List<Celda>(100);
            analizador = new Analizador();
            LlenarCeldas();
            AgregarAcorazado();
            AgregarCruceros();
            AgregarDestructores();
            AgregarSubmarinos();
        }
        #endregion

        #region Metodos
        /// <summary>
        /// Devuelve la celda especificada
        /// </summary>
        /// <param name="fila"></param>
        /// <param name="columna"></param>
        /// <returns></returns>
        public Celda ObtenerCelda(int fila, int columna)
        {
            foreach (Celda celda in celdas)
            {
                if (celda.Fila==fila && celda.Columna==columna)
                {
                    return celda;
                }
            }
            return null;
        }

        /// <summary>
        /// Devuelve la posición de la celda especificada
        /// </summary>
        /// <param name="fila"></param>
        /// <param name="columna"></param>
        /// <returns></returns>
        public int ObtenerIndex(int fila, int columna)
        {
            foreach (Celda celda in celdas)
            {
                if (celda.Fila == fila && celda.Columna == columna)
                {
                    return celdas.IndexOf(celda);
                }
            }
            return -1;
        }

        /// <summary>
        /// Llena todo el tablero de celdas de tipo agua
        /// </summary>
        private void LlenarCeldas()
        {
            for (int fila=0;fila<10;fila++)
            {
                for (int columna=0;columna<10;columna++)
                {
                    celdas.Add(new CeldaAgua(fila, columna));
                }
            }
        }

        /// <summary>
        /// Comprueba si existe alguna celda que tenga las mismas características
        /// </summary>
        /// <param name="celdaBuscar">Celda a buscar</param>
        /// <returns></returns>
        public bool BuscarCeldaID(Celda celdaBuscar)
        {
            string tipo = celdaBuscar.GetType().Name;
            int id = celdaBuscar.ID;
            foreach (Celda celda in celdas)
            {
                string tipoEncontrado = celda.GetType().Name;
                if (tipo == tipoEncontrado && celda.Activo && celda.ID == id)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Agrega los acorazados al tablero
        /// </summary>
        private void AgregarAcorazado()
        {
            int espacio = 4;
            int id = 0;
            int[,] celdasAcorazado = analizador.ObtenerCeldas(espacio);
            for (int i=0;i<celdasAcorazado.GetLength(0);i++)
            {
                int fila = celdasAcorazado[i, 0];
                int columna = celdasAcorazado[i, 1];
                int index = ObtenerIndex(fila, columna);
                celdas[index] = new CeldaAcorazado(fila, columna, id);
                if (i==3)
                {
                    id++;
                }
            }
        }

        /// <summary>
        /// Agrega los cruceros al tablero
        /// </summary>
        private void AgregarCruceros()
        {
            int espacio = 3;
            int[,] celdasCrucero = analizador.ObtenerCeldas(espacio);
            int id = 1;
            for (int i = 0; i < celdasCrucero.GetLength(0); i++)
            {
                int fila = celdasCrucero[i, 0];
                int columna = celdasCrucero[i, 1];
                int index = ObtenerIndex(fila, columna);
                celdas[index] = new CeldaCrucero(fila, columna, id);
                if (i == 2 || i==5)
                {
                    id++;
                }
            }
        }

        /// <summary>
        /// Agrega los destructores al tablero
        /// </summary>
        private void AgregarDestructores()
        {
            int espacio = 2;
            int[,] celdasDestructores = analizador.ObtenerCeldas(espacio);
            int id = 1;
            for (int i = 0; i < celdasDestructores.GetLength(0); i++)
            {
                int fila = celdasDestructores[i, 0];
                int columna = celdasDestructores[i, 1];
                int index = ObtenerIndex(fila, columna);
                celdas[index] = new CeldaDestructor(fila, columna, id);
                if (i == 1 || i == 3 || i== 5)
                {
                    id++;
                }
            }
        }

        /// <summary>
        /// Agrega los submarinos al tablero
        /// </summary>
        private void AgregarSubmarinos()
        {
            int espacio = 1;
            int[,] celdasSubmarinos = analizador.ObtenerCeldas(espacio);
            for (int i = 0; i < celdasSubmarinos.GetLength(0); i++)
            {
                int fila = celdasSubmarinos[i, 0];
                int columna = celdasSubmarinos[i, 1];
                int index = ObtenerIndex(fila, columna);
                celdas[index] = new CeldaSubmarino(fila, columna);
            }
        }

        #endregion

        #region Propiedades
        /// <summary>
        /// Devuelve los submarinos restantes
        /// </summary>
        public int Submarinos
        {
            get
            {
                return submarinos;
            }
            set
            {
                submarinos = value;
            }
        }

        /// <summary>
        /// Devuelve los destructores restantes
        /// </summary>
        public int Destructores
        {
            get
            {
                return destructores;
            }
            set
            {
                destructores = value;
            }
        }

        /// <summary>
        /// Devuelve los cruceros restantes
        /// </summary>
        public int Cruceros
        {
            get
            {
                return cruceros;
            }
            set
            {
                cruceros = value;
            }
        }

        /// <summary>
        /// Devuelve los acorazados restantes
        /// </summary>
        public int Acorazados
        {
            get
            {
                return acorazados;
            }
            set
            {
                acorazados = value;
            }
        }
        #endregion
    }
}
