﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Media;
using System.Speech.Synthesis;

namespace Problema1
{
    public partial class Form1 : Form
    {
        Juego juego;
        Juego juego2;
        int juegoActual;
        readonly Point localizacionEstado;
        readonly SoundPlayer soundAgua;
        readonly SoundPlayer soundTocado;
        readonly SoundPlayer soundHundido;
        SpeechSynthesizer synth;
        Prompt leer = new Prompt("Juego Hundir la flota");
        Datos datos;

        /// <summary>
        /// Constructor base
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            datos = Program.datos;
            juego = Program.juego;
            SeleccionarJugadores();
            juegoActual = 1;
            #region imagenBloqueo
            Size dimensionbloqueo = new Size(549, 404);
            Point puntobloqueo = new Point(395, 214);
            bloqueo1.Size = dimensionbloqueo;
            bloqueo1.Location = puntobloqueo;
            bloqueo2.Size = dimensionbloqueo;
            bloqueo2.Location = puntobloqueo;
            bloqueo2.Visible = true;
            #endregion
            #region Localizacion estados
            localizacionEstado = new Point(46, 372);
            panelHundido.Location = localizacionEstado;
            panelTocado.Location = localizacionEstado;
            panelAgua.Location = localizacionEstado;
            panelHundido2.Location = localizacionEstado;
            panelTocado2.Location = localizacionEstado;
            panelAgua2.Location = localizacionEstado;
            #endregion
            #region Sonido
            soundAgua = new SoundPlayer(Properties.Resources.sonidoAgua);
            soundTocado = new SoundPlayer(Properties.Resources.sonidoTocado);
            soundHundido = new SoundPlayer(Properties.Resources.sonidoHundido);
            #endregion
            #region Voz
            synth = new SpeechSynthesizer();
            synth.SetOutputToDefaultAudioDevice();
            synth.Volume = 90;
            synth.SpeakAsync(leer);
            #endregion
            ActualizarFormulario();
        }

        /// <summary>
        /// Cambia el color de la celda al pasar por encima
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Seleccionado(object sender, EventArgs e)
        {
            TableLayoutPanelCellPosition posicion = contenedor.GetCellPosition((Control)sender);
            Celda celda;
            if (juegoActual==1)
            {
                celda = juego.TableroJuego.ObtenerCelda(posicion.Row, posicion.Column);
            }
            else
            {
                celda = juego2.TableroJuego.ObtenerCelda(posicion.Row, posicion.Column);
            }
            if (celda.Activo)
            {
                PictureBox imagen = (PictureBox)sender;
                imagen.BackColor = Color.LemonChiffon;
                imagen.Cursor = Cursors.Hand;
            }
        }

        /// <summary>
        /// Cambia el color de la celda al pasar por encima
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Deseleccionado(object sender, EventArgs e)
        {
            TableLayoutPanelCellPosition posicion = contenedor.GetCellPosition((Control)sender);
            Celda celda;
            if (juegoActual == 1)
            {
                celda = juego.TableroJuego.ObtenerCelda(posicion.Row, posicion.Column);
            }
            else
            {
                celda = juego2.TableroJuego.ObtenerCelda(posicion.Row, posicion.Column);
            }
            if (celda.Activo)
            {
                PictureBox imagen = (PictureBox)sender;
                imagen.BackColor = Color.LightCyan;
                imagen.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Desactiva una celda al clickar en ella y muestra el resultado
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Accionar(object sender, MouseEventArgs e)
        {
            TableLayoutPanelCellPosition posicion = contenedor.GetCellPosition((Control)sender);
            Celda celda;
            if (juegoActual == 1)
            {
                celda = juego.TableroJuego.ObtenerCelda(posicion.Row, posicion.Column);
            }
            else
            {
                celda = juego2.TableroJuego.ObtenerCelda(posicion.Row, posicion.Column);
            }
            PictureBox imagen = (PictureBox)sender;
            string tipo = celda.GetType().Name;
            if (celda.Activo)
            {
                celda.Activo = false;
                string estado = celda.Mostrar(this,juegoActual);
                switch (estado)
                {
                    case "Tocado":
                        if (juegoActual==1 || juego2==null)
                        {
                            panelAgua.Visible = false;
                            panelHundido.Visible = false;
                            panelTocado.Visible = true;
                        }
                        else
                        {
                            panelAgua2.Visible = false;
                            panelHundido2.Visible = false;
                            panelTocado2.Visible = true;
                        }
                        soundTocado.Play();
                        break;
                    case "Hundido":
                        if (juegoActual == 1 || juego2 == null)
                        {
                            panelAgua.Visible = false;
                            panelHundido.Visible = true;
                            panelTocado.Visible = false;
                        }
                        else
                        {
                            panelAgua2.Visible = false;
                            panelHundido2.Visible = true;
                            panelTocado2.Visible = false;
                        }
                        soundHundido.Play();
                        break;
                    case "Agua":
                        if (juegoActual == 1 || juego2 == null)
                        {
                            panelAgua.Visible = true;
                            panelHundido.Visible = false;
                            panelTocado.Visible = false;
                        }
                        else
                        {
                            panelAgua2.Visible = true;
                            panelHundido2.Visible = false;
                            panelTocado2.Visible = false;
                        }
                        soundAgua.Play();
                        break;
                }
                if (tipo!="CeldaAgua")
                {
                    imagen.Image = Properties.Resources.fuego;
                    imagen.BackColor = Color.DarkOrange;
                    if (estado == "Tocado")
                    {
                        if (juegoActual==1)
                        {
                            juego.Puntuacion += 5;
                        }
                        else
                        {
                            juego2.Puntuacion += 5;
                        }
                    }
                    else
                    {
                        if (juegoActual==1)
                        {
                            juego.Puntuacion += 15;
                        }
                        else
                        {
                            juego2.Puntuacion += 15;
                        }
                    }
                    ActualizarFormulario();
                }
                else
                {
                    imagen.Image = Properties.Resources.agua;
                    if (juego2 != null)
                    {
                        if (juegoActual == 1)
                        {
                            juego.Puntuacion -= 1;
                            ActualizarFormulario();
                            juegoActual = 2;
                            bloqueo1.Visible = true;
                            bloqueo2.Visible = false;
                            tabControl1.SelectedIndex = 1;
                        }
                        else
                        {
                            juego2.Puntuacion -= 1;
                            ActualizarFormulario();
                            juegoActual = 1;
                            bloqueo1.Visible = false;
                            bloqueo2.Visible = true;
                            tabControl1.SelectedIndex = 0;
                        }
                    }
                    else
                    {
                        juego.Puntuacion -= 1;
                        ActualizarFormulario();
                    }
                }
                ComprobarPuntuacion();
            }
        }

        /// <summary>
        /// Muestra el formulario de selección de jugadores
        /// </summary>
        private void SeleccionarJugadores()
        {
            FormJugadores formJug = new FormJugadores();
            formJug.ShowDialog();
            if (Program.jugadores==2)
            {
                Program.juego2 = new Juego();
                juego2 = Program.juego2;
            }
            else
            {
                tabControl1.TabPages.Remove(tabJugador2);
            }
        }

        /// <summary>
        /// Muestra todos los barcos del tablero al rendirse
        /// </summary>
        private void MostrarBarcos()
        {
            TableLayoutPanel panelContenedor;
            if (juegoActual==1 || juego2 == null)
            {
                panelContenedor = contenedor;
            }
            else
            {
                panelContenedor = contenedor2;
            }
            foreach (Control control in panelContenedor.Controls)
            {
                TableLayoutPanelCellPosition posicion = panelContenedor.GetCellPosition(control);
                Celda celda;
                if (juegoActual == 1 || juego2 == null)
                {
                    celda = juego.TableroJuego.ObtenerCelda(posicion.Row, posicion.Column);
                }
                else
                {
                    celda = juego2.TableroJuego.ObtenerCelda(posicion.Row, posicion.Column);
                }
                PictureBox imagen = (PictureBox)control;
                if (!(celda is CeldaAgua))
                {
                    if (celda is CeldaAcorazado)
                    {
                        imagen.BackColor = Color.Pink;
                    }
                    else if (celda is CeldaCrucero)
                    {
                        imagen.BackColor = Color.Red;
                    }
                    else if (celda is CeldaDestructor)
                    {
                        imagen.BackColor = Color.Yellow;
                    }
                    else
                    {
                        imagen.BackColor = Color.Blue;
                    }
                }
            }
        }

        /// <summary>
        /// Actualiza los labels del formulario
        /// </summary>
        private void ActualizarFormulario()
        {
            if (juegoActual == 1 || juego2==null)
            {
                Tablero tablero = juego.TableroJuego;
                labelSubmarino.Text = tablero.Submarinos.ToString();
                labelAcorazado.Text = tablero.Acorazados.ToString();
                labelCrucero.Text = tablero.Cruceros.ToString();
                labelDestructor.Text = tablero.Destructores.ToString();
                int puntos = juego.Puntuacion;
                int puntosLabel = int.Parse(labelPuntos.Text);
                if (puntosLabel <= puntos)
                {
                    labelPuntos.Text = puntos.ToString();
                    labelPuntos.ForeColor = Color.Green;
                }
                else
                {
                    labelPuntos.Text = puntos.ToString();
                    labelPuntos.ForeColor = Color.Crimson;
                }
            }
            else
            {
                Tablero tablero = juego2.TableroJuego;
                labelSubmarino2.Text = tablero.Submarinos.ToString();
                labelAcorazado2.Text = tablero.Acorazados.ToString();
                labelCrucero2.Text = tablero.Cruceros.ToString();
                labelDestructor2.Text = tablero.Destructores.ToString();
                int puntos = juego2.Puntuacion;
                int puntosLabel = int.Parse(labelPuntos2.Text);
                if (puntosLabel <= puntos)
                {
                    labelPuntos2.Text = puntos.ToString();
                    labelPuntos2.ForeColor = Color.Green;
                }
                else
                {
                    labelPuntos2.Text = puntos.ToString();
                    labelPuntos2.ForeColor = Color.Crimson;
                }
            }
        }

        /// <summary>
        /// Actualiza las variables que contienen la cantidad de barcos que quedan
        /// </summary>
        /// <param name="tipo">indica el tipo de barco</param>
        public void ActualizarCantidadBarcos(string tipo)
        {
            Tablero tablero;
            if (juegoActual == 1 || juego2 == null)
            {
                tablero = juego.TableroJuego;
            }
            else
            {
                tablero = juego2.TableroJuego;
            }
            switch (tipo)
            {
                case "CeldaAcorazado":
                    tablero.Acorazados--;
                    break;
                case "CeldaDestructor":
                    tablero.Destructores--;
                    break;
                case "CeldaCrucero":
                    tablero.Cruceros--;
                    break;
                case "CeldaSubmarino":
                    tablero.Submarinos--;
                    break;
            }
        }

        /// <summary>
        /// Comprueba la puntuación. Finaliza el juego en caso de rendirse o ganar y muestra el menu de registro de puntuación.
        /// </summary>
        private void ComprobarPuntuacion()
        {
            bool acabar = false;
            Tablero tablero;
            if (juego2!=null)
            {
                if (juegoActual==1)
                {
                    tablero = juego.TableroJuego;
                }
                else
                {
                    tablero = juego2.TableroJuego;
                }
                if (juego.Puntuacion == 20 || juego2.Puntuacion == 20)
                {
                    FormPierde formulario = new FormPierde();
                    formulario.ShowDialog();
                    acabar = true;
                }
                if (tablero.Destructores == 0 && tablero.Acorazados == 0 && tablero.Cruceros == 0 && tablero.Submarinos == 0)
                {
                    FormGana formulario = new FormGana();
                    formulario.ShowDialog();
                    acabar = true;
                }
                if (acabar)
                {
                    bool datosGuardados = false;
                    int puntuacionJ1 = juego.Puntuacion;
                    int puntuacionJ2 = juego2.Puntuacion;
                    if (puntuacionJ1>datos.PuntuacionMasBaja)
                    {
                        Registro registro = new Registro(puntuacionJ1);
                        registro.ShowDialog();
                        datosGuardados = true;
                    }
                    if (puntuacionJ2 > datos.PuntuacionMasBaja)
                    {
                        Registro registro = new Registro(puntuacionJ2);
                        registro.ShowDialog();
                        datosGuardados = true;
                    }
                    if (datosGuardados)
                    {
                        datos.EscribirDatos();
                    }
                    Application.Exit();
                }
            }
            else
            {
                tablero = juego.TableroJuego;
                if (juego.Puntuacion == 20)
                {
                    FormPierde formulario = new FormPierde();
                    formulario.ShowDialog();
                    acabar = true;
                }
                if (tablero.Destructores == 0 && tablero.Acorazados == 0 && tablero.Cruceros == 0 && tablero.Submarinos == 0)
                {
                    FormGana formulario = new FormGana();
                    formulario.ShowDialog();
                    acabar = true;
                }
                if (acabar)
                {
                    int puntuacionJ1 = juego.Puntuacion;
                    if (puntuacionJ1 > datos.PuntuacionMasBaja)
                    {
                        Registro registro = new Registro(puntuacionJ1);
                        registro.ShowDialog();
                        datos.EscribirDatos();
                    }
                    Application.Exit();
                }
            }
        }

        /// <summary>
        /// Cambia el cursor al pasar sobre la imagen de rendirse
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void imagenRendirse_MouseEnter(object sender, EventArgs e)
        {
            PictureBox imagen = (PictureBox)sender;
            imagen.Image = Properties.Resources.RendirseON;
            imagen.Cursor = Cursors.Hand;
        }

        /// <summary>
        /// Cambia el cursor al pasar sobre la imagen de rendirse
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void imagenRendirse_MouseLeave(object sender, EventArgs e)
        {
            PictureBox imagen = (PictureBox)sender;
            imagen.Image = Properties.Resources.RendirseOFF;
            imagen.Cursor = Cursors.Default;
        }

        /// <summary>
        /// Finaliza el juego y muestra el menú de registro de puntuación si es necesario.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void imagenRendirse_MouseClick(object sender, MouseEventArgs e)
        {
            MostrarBarcos();
            FormPierde formulario = new FormPierde();
            formulario.ShowDialog();
            if (juego2 != null)
            {
                bool datosGuardados = false;
                int puntuacionJ1 = juego.Puntuacion;
                int puntuacionJ2 = juego2.Puntuacion;
                if (puntuacionJ1 > datos.PuntuacionMasBaja)
                {
                    Registro registro = new Registro(puntuacionJ1);
                    registro.ShowDialog();
                    datosGuardados = true;
                }
                if (puntuacionJ2 > datos.PuntuacionMasBaja)
                {
                    Registro registro = new Registro(puntuacionJ2);
                    registro.ShowDialog();
                    datosGuardados = true;
                }
                if (datosGuardados)
                {
                    datos.EscribirDatos();
                }
            }
            else
            {
                int puntuacionJ1 = juego.Puntuacion;
                if (puntuacionJ1 > datos.PuntuacionMasBaja)
                {
                    Registro registro = new Registro(puntuacionJ1);
                    registro.ShowDialog();
                    datos.EscribirDatos();
                }
            }
            Application.Exit();
        }

        /// <summary>
        /// Cambia el cursor al pasar sobre la imagen del Hall of fame
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void botonHall_MouseEnter(object sender, EventArgs e)
        {
            botonHall.Cursor = Cursors.Hand;
            botonHall.Image = Properties.Resources.botonHallOn;
        }

        /// <summary>
        /// Cambia el cursor al pasar sobre la imagen del Hall of fame
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void botonHall_MouseLeave(object sender, EventArgs e)
        {
            botonHall.Cursor = Cursors.Default;
            botonHall.Image = Properties.Resources.botonHallOFF;
        }

        /// <summary>
        /// Muestra el Hall of Fame al hacer click en su imagen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void botonHall_Click(object sender, EventArgs e)
        {
            HallFame formulario = new HallFame();
            formulario.ShowDialog();
        }
    }
}
