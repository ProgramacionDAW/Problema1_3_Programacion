﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Problema1
{
    public partial class Registro : Form
    {
        protected string avatar;
        protected int puntuacion;
        int letra1 = 1;
        int letra2 = 1;
        int letra3 = 1;
        Datos datos;
        /// <summary>
        /// Constructor usado para mostrar el formulario de registro
        /// </summary>
        /// <param name="puntuacion">puntuación obtenida final</param>
        public Registro(int puntuacion)
        {
            InitializeComponent();
            RellenarAvatares();
            this.puntuacion = puntuacion;
            lPuntuacion.Text = puntuacion.ToString();
            datos = Program.datos;
        }

        /// <summary>
        /// Cambia el cursor al pasar por encima del control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AvatarEntra(object sender, EventArgs e)
        {
            PictureBox imagen = (PictureBox)sender;
            imagen.Cursor = Cursors.Hand;
        }

        /// <summary>
        /// Cambia el cursor al pasar por encima del control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AvatarSale(object sender, EventArgs e)
        {
            PictureBox imagen = (PictureBox)sender;
            imagen.Cursor = Cursors.Default;
        }

        /// <summary>
        /// Cambia la imagen del usuario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AvatarClick(object sender, MouseEventArgs e)
        {
            PictureBox imagen = (PictureBox)sender;
            avatar = imagen.Tag.ToString();
            PictSelec.Image = imagen.Image;
        }

        /// <summary>
        /// Agrega un tag a cada imagen
        /// </summary>
        private void RellenarAvatares()
        {
            int contador = 0;
            foreach(Control control in contenedorAvatar.Controls)
            {
                PictureBox imagen = control as PictureBox;
                if (imagen!=null)
                {
                    contador++;
                    string recurso = string.Concat("a", contador);
                    imagen.Image = (Image)Properties.Resources.ResourceManager.GetObject(recurso);
                    imagen.Tag = recurso;
                }
            }
        }

        /// <summary>
        /// Aumenta en uno la letra que muestra
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AumentaLetra(object sender, MouseEventArgs e)
        {
            Panel boton = (Panel)sender;
            string recurso;
            switch(boton.Name)
            {
                case "aumenta1":
                    letra1++;
                    if(letra1>26)
                    {
                        letra1 = 26;
                    }
                    recurso = string.Concat("l", letra1);
                    picLetra1.Image = (Image)Properties.Resources.ResourceManager.GetObject(recurso);
                    break;
                case "aumenta2":
                    letra2++;
                    if (letra2 > 26)
                    {
                        letra2 = 26;
                    }
                    recurso = string.Concat("l", letra2);
                    picLetra2.Image = (Image)Properties.Resources.ResourceManager.GetObject(recurso);
                    break;
                case "aumenta3":
                    letra3++;
                    if (letra3 > 26)
                    {
                        letra3 = 26;
                    }
                    recurso = string.Concat("l", letra3);
                    picLetra3.Image = (Image)Properties.Resources.ResourceManager.GetObject(recurso);
                    break;
            }
        }

        /// <summary>
        /// Disminuye en uno la letra que muestra
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Disminuyeletra(object sender, MouseEventArgs e)
        {
            Panel boton = (Panel)sender;
            string recurso;
            switch (boton.Name)
            {
                case "baja1":
                    letra1--;
                    if(letra1<1)
                    {
                        letra1 = 1;
                    }
                    recurso = string.Concat("l", letra1);
                    picLetra1.Image = (Image)Properties.Resources.ResourceManager.GetObject(recurso);
                    break;
                case "baja2":
                    letra2--;
                    if (letra2 < 1)
                    {
                        letra2 = 1;
                    }
                    recurso = string.Concat("l", letra2);
                    picLetra2.Image = (Image)Properties.Resources.ResourceManager.GetObject(recurso);
                    break;
                case "baja3":
                    letra3--;
                    if (letra3 < 1)
                    {
                        letra3 = 1;
                    }
                    recurso = string.Concat("l", letra3);
                    picLetra3.Image = (Image)Properties.Resources.ResourceManager.GetObject(recurso);
                    break;
            }
        }

        /// <summary>
        /// Obtiene el caracter en función a su posición
        /// </summary>
        /// <param name="numero">posición de la letra</param>
        /// <returns></returns>
        private string ObtenerLetra(int numero)
        {
            string[] letras = new string[26] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
            return letras[numero-1];
        }

        /// <summary>
        /// Agrega al registro el jugador y cierra el formulario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void botonGuardar_Click(object sender, EventArgs e)
        {
            if (avatar=="" || avatar==null)
            {
                avatar = "interrogante";
            }
            datos.AgregarRegistro(ObtenerLetra(letra1) + ObtenerLetra(letra2) + ObtenerLetra(letra3), avatar, puntuacion);
            Close();
        }
    }
}
